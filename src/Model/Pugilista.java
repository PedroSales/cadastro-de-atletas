package Model;

public class Pugilista extends Atleta{

	private String categoria;
	private String envergadura;
	private int QntNocautes;
	private int QntDerrotas;
	private int QntVitorias;
	
	public Pugilista( String nome, String categoria){
		super(nome);
		this.categoria = categoria;
	}
	
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getEnvergadura() {
		return envergadura;
	}
	public void setEnvergadura(String envergadura) {
		this.envergadura = envergadura;
	}
	public int getQntNocautes() {
		return QntNocautes;
	}
	public void setQntNocautes(int qntNocautes) {
		QntNocautes = qntNocautes;
	}
	public int getQntDerrotas() {
		return QntDerrotas;
	}
	public void setQntDerrotas(int qntDerrotas) {
		QntDerrotas = qntDerrotas;
	}
	public int getQntVitorias() {
		return QntVitorias;
	}
	public void setQntVitorias(int qntVitorias) {
		QntVitorias = qntVitorias;
	}
	
	

}
