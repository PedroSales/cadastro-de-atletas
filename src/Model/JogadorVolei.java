package Model;

public class JogadorVolei extends Atleta {

	private String AlturaDoPulo;
	private String VelocidadeDoSaque;
	private String Posicao;
	

	public JogadorVolei(String nome, String Posicao) {
		super(nome);
		this.Posicao = Posicao;
	}
	
	
	public String getAlturaDoPulo() {
		return AlturaDoPulo;
	}
	public void setAlturaDoPulo(String alturaDoPulo) {
		AlturaDoPulo = alturaDoPulo;
	}
	public String getVelocidadeDoSaque() {
		return VelocidadeDoSaque;
	}
	public void setVelocidadeDoSaque(String velocidadeDoSaque) {
		VelocidadeDoSaque = velocidadeDoSaque;
	}
	public String getPosicao() {
		return Posicao;
	}
	public void setPosicao(String posicao) {
		Posicao = posicao;
	}
	
	

}
