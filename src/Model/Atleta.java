package Model;

public class Atleta {

	private String nome;
	private String idade;
	private String Altura;
	private String peso;
	private Endereco endereco;
	
	
	public Atleta (String nome)
	{
		this.nome = nome;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	public String getPeso() {
		return peso;
	}
	public String getAltura() {
		return Altura;
	}

	public void setAltura(String altura) {
		Altura = altura;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}


}