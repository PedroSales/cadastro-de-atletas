package View;

import java.util.Scanner;

import Controller.ControladorAtleta;
import Model.Atleta;
import Model.Endereco;

public class TelaAtleta {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		int opcao = -1;
		Scanner leitor = null;
		Atleta umAtleta = null;
		ControladorAtleta umControlador = new ControladorAtleta();
	
	
	while(opcao != 0){
		leitor = new Scanner(System.in);
		
		System.out.println("Menu de Opções");
	    System.out.println("\n");
	    System.out.println("Para adicionar um atleta digite 1: ");
	    System.out.println("Para buscar um atleta digite 2: ");
	    System.out.println("Para remover um atleta digite 3: ");
	    System.out.println("Para parar o programa digite 0.");
	       
	
	    opcao= leitor.nextInt();
	 	
	    leitor.nextLine();
		
		switch(opcao){
		
		case 1:
		System.out.println("Digite o Nome do Atleta: ");
		umAtleta = new Atleta(leitor.next());
		
		System.out.println("Digite a Idade do Atleta: ");
		umAtleta.setIdade(leitor.next());
		
		System.out.println("Digite a Altura do Atleta: ");
		umAtleta.setAltura(leitor.next());
	
		System.out.println("Digite o Peso do Atleta: ");
		umAtleta.setPeso(leitor.next());
		
		System.out.println("Digite o CEP:");
		Endereco umEndereco =  new Endereco(leitor.next());
		
		System.out.println("Digite Logradouro:");
		umEndereco.setLogradouro(leitor.next());
		
		System.out.println("Digite o Numero: ");
		umEndereco.setNumero(leitor.next());
	
		System.out.println("Digite o Bairro:");
		umEndereco.setBairro(leitor.next());
		
		System.out.println("Digite o Cidade: ");
		umEndereco.setCidade(leitor.next());
		
		System.out.println("Digite o Estado: ");
		umEndereco.setEstado(leitor.next());
		
		umAtleta.setEndereco(umEndereco);
		
		umControlador.adicionar(umAtleta);
		
		break;
		
		case 2:
		 
			            	
		System.out.println("Digite o nome do Atleta que deseja buscar: ");
	  	String nomeAtleta = leitor.nextLine();
	    Atleta outroAtleta = null;
	    outroAtleta = umControlador.buscar(nomeAtleta);
	             	
	
		     	if(umControlador.buscar(nomeAtleta) != null){
	
	            		System.out.println("Idade: " + outroAtleta.getIdade());
	            		System.out.println("Altura: " + outroAtleta.getAltura());
	            		System.out.println("Peso: " + outroAtleta.getPeso());        		
	            		System.out.println("Cep: " + outroAtleta.getEndereco().getCEP());
	            		System.out.println("País: " + outroAtleta.getEndereco().getPais());
	            		System.out.println("Estado: " + outroAtleta.getEndereco().getEstado());
	            		System.out.println("Cidade: " + outroAtleta.getEndereco().getCidade());
	            		System.out.println("Bairro: " + outroAtleta.getEndereco().getBairro());
	            		System.out.println("Logradouro: " + outroAtleta.getEndereco().getLogradouro());
	            		System.out.println("Número: " + outroAtleta.getEndereco().getNumero());
	
		            		
			            	}
			            	
	
	           	else {
	            		System.out.println("Atleta não cadastrado");
	
		            	}
	
	         	
		          	break;
	
		          	
		case 3:
	           	
	            System.out.println("Digite o nome do atleta que deseja remover: ");
	           	nomeAtleta = leitor.nextLine();
	          	
	           	outroAtleta = null;
	           	
	           	outroAtleta = umControlador.buscar(nomeAtleta);
	            	
	           	if(umControlador.buscar(nomeAtleta) != null){
	
	           		umControlador.remover(outroAtleta);
	
		            System.out.println("Atleta removido com Sucesso");
	
		            	}
	
		        else
		
	         		System.out.println("Atleta nao cadastrado");          	
		
		
	           	break;
		
			    
		case 0: 
			            	
		      	
			System.out.println("Programa Encerrado");
			            	
		     
			break;
		    	
		
			
		default:
		
			            	
		           	System.out.println("Opção Inexistente");
		}
		}
	

}


}
