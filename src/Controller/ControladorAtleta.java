package Controller;

import java.util.ArrayList;

import Model.Atleta;

public class ControladorAtleta {

	ArrayList <Atleta> listaAtletas;
	
		
		public ControladorAtleta(){
			listaAtletas = new ArrayList<Atleta>();
		}
		
		public void adicionar (Atleta umAtleta){
			listaAtletas.add(umAtleta);
		}
		
		public void remover (Atleta umAtleta){
			listaAtletas.remove(umAtleta);
		}
		
		public Atleta buscar (String umNome){
			for(Atleta umAtleta : listaAtletas){
				if(umAtleta.getNome().equalsIgnoreCase(umNome));
				return umAtleta;
			}
			
			return null;
		}

}
